package ru.dymeth.deathgames.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.dymeth.deathgames.DeathGames;
import ru.dymeth.deathgames.arena.GameDeathArena;

public class ArenaListener implements Listener {
    private final GameDeathArena arena;

    public ArenaListener(DeathGames plugin) {
        this.arena = plugin.getArena();
    }

    @EventHandler
    private void on(AsyncPlayerChatEvent event) {
        if (!event.getMessage().startsWith("!")) return;
        event.setMessage(event.getMessage().substring(1));
        if (event.getMessage().isEmpty()) event.setCancelled(true);
    }

    @EventHandler
    private void on(PlayerLoginEvent event) {
        String message = this.arena.getConnectDenyMessage();
        if (message == null) return;
        event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
        event.setKickMessage(message);
    }

    @EventHandler
    private void on(PlayerJoinEvent event) {
        this.arena.addPlayer(event.getPlayer());
        event.setJoinMessage(null);
    }

    @EventHandler
    private void on(PlayerQuitEvent event) {
        this.arena.removePlayer(event.getPlayer());
        event.setQuitMessage(null);
    }
}
