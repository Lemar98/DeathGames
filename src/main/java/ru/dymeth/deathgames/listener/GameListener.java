package ru.dymeth.deathgames.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;
import ru.dymeth.deathgames.DeathGames;
import ru.dymeth.deathgames.arena.GameDeathArena;
import ru.dymeth.deathgames.arena.GameState;
import ru.dymeth.deathgames.item.ItemManager;
import ru.dymeth.deathgames.item.UsableItem;
import ru.dymeth.deathgames.util.EntityUtils;
import ru.dymeth.deathgames.util.ItemUtil;
import us.blockbox.biomefinder.event.CacheBuildCompleteEvent;

public class GameListener implements Listener {
    private final GameDeathArena arena;
    private final ItemManager itemManager;

    public GameListener(DeathGames plugin) {
        this.arena = plugin.getArena();
        this.itemManager = plugin.getItemManager();
    }

    @EventHandler
    private void on(CacheBuildCompleteEvent event) {
        this.arena.findMiddleOf(event.getWorld());
    }

    @EventHandler
    private void on(PlayerInteractEvent event) {
        ItemStack stack = event.getItem();
        if (stack == null) return;
        String id = ItemUtil.getID(stack);
        if (id.isEmpty()) return;
        UsableItem usableItem = this.itemManager.getUsableItem(id);
        if (usableItem == null) throw new IllegalStateException("Не найден предмет с айди " + id);
        if (!usableItem.onUse(event)) return;
        event.getPlayer().getInventory().setItemInMainHand(null);
    }

    @EventHandler
    private void on(PlayerDeathEvent event) {
        event.setKeepInventory(true);
        if (this.arena.getVictim() != event.getEntity()) {
            event.setDeathMessage(null);
            this.respawn(event.getEntity());
            return;
        }
        if (!this.arena.getTarget().isCauseOf(event.getEntity())) {
            this.respawn(event.getEntity());
            return;
        }
        if (this.arena.getTarget().isShouldDie()) {
            event.getEntity().sendMessage(ChatColor.GREEN + "Вы выполнили задание!");
            this.arena.endGame(true);
        } else {
            event.getEntity().sendMessage(ChatColor.RED + "Вы провалили задание!");
            this.arena.endGame(false);
        }
    }

    private void respawn(Player player) {
        player.spigot().respawn();
        this.arena.sendTargetMessage(player);
        player.setNoDamageTicks(20 * 5);
    }

    @EventHandler
    private void on(PlayerPortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    private void on(PlayerSpawnLocationEvent event) {
        event.setSpawnLocation(this.arena.getSpawnLocation());
    }

    @EventHandler
    private void on(PlayerRespawnEvent event) {
        event.setRespawnLocation(this.arena.getSpawnLocation());
        if (this.arena.getState() == GameState.END)
            event.getPlayer().getInventory().clear();
    }

    @EventHandler
    private void on(PlayerDropItemEvent event) {
        boolean victim = this.arena.getVictim() == event.getPlayer();
        if (!this.arena.getKitCooldowns(victim, false)
                .containsKey(event.getItemDrop().getItemStack())) return;
        event.setCancelled(true);
    }

    @EventHandler
    private void on(EntityDamageByEntityEvent event) {
        Entity damaged = event.getEntity();
        if (!(damaged instanceof Player)) return;
        Entity damager = EntityUtils.getSourceDamager(event.getDamager());
        if (!(damager instanceof Player)) return;
        if (damaged == damager) return; // Самоурон
        Entity victim = this.arena.getVictim();
        if (damaged == victim || damager == victim) {
            // Запрет PVP с жертвой
            if (damaged == victim) event.setDamage(0);
            return;
        }
        event.setCancelled(true); // Запрет PVP между участниками команды
    }

    @EventHandler
    private void on(EntityTargetEvent event) {
        if (event.getTarget() == null) return;
        if (event.getTarget() == this.arena.getVictim()) return;
        event.setCancelled(true);
    }
}
