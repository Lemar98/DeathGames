package ru.dymeth.deathgames.listener;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import ru.dymeth.deathgames.DeathGames;

public abstract class CustomListener implements Listener {
    private final DeathGames plugin = JavaPlugin.getPlugin(DeathGames.class);

    protected DeathGames getPlugin() {
        return this.plugin;
    }
}
