package ru.dymeth.deathgames.util;

import com.google.common.base.Preconditions;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import us.blockbox.biomefinder.BiomeFinder;
import us.blockbox.biomefinder.CacheBuilder;
import us.blockbox.biomefinder.Coord;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;

public class LocationUtils {
    public static int getHighestBlockYAt(Location location, boolean allowBukkitCache, boolean ignoreNetherRoof) {
        return getHighestBlockAt(location, allowBukkitCache, ignoreNetherRoof).getY();
    }

    /**
     * Баккитовский метод зачастую возвращает некорректные значения.
     * При ручной замене блока в столбце возвращаемое значение увеличивается до высоты этого блока,
     * но блок, созданные генератором, по-прежнему не учитываются.
     * Данное поведение наблюдалось как минимум в Нижнем Мире
     */
    public static Block getHighestBlockAt(Location location, boolean allowBukkitCache, boolean ignoreNetherRoof) {
        World world = location.getWorld();
        int x = location.getBlockX();
        int z = location.getBlockZ();
        if (!world.isChunkLoaded(x >> 4, z >> 4))
            throw new IllegalArgumentException("Невозможно получить высоту в незагруженном чанке");
        if (allowBukkitCache) {
            Block result = world.getHighestBlockAt(x, z);
            if (result.getY() > 0) return result; // Результат уже находится в кэше
        }
        Chunk chunk = world.getChunkAt(x >> 4, z >> 4);
        x = x & 15; // Блок в чанке
        z = z & 15; // Блок в чанке
        // TODO Заносить значение в кэш
        for (int y = ignoreNetherRoof ? 80 : 255; y >= 0; y--) {
            Material material = chunk.getBlock(x, y, z).getType();
            if (material.isSolid() || isLiquid(material)) return chunk.getBlock(x, y, z);
        }
        return chunk.getBlock(x, 0, z);
    }

    private static boolean isLiquid(Material material) {
        switch (material) {
            case WATER:
            case STATIONARY_WATER:
            case LAVA:
            case STATIONARY_LAVA:
                return true;
            default:
                return false;
        }
    }

    public static boolean teleportWithoutEvent(Player player, Location to) {
        Preconditions.checkArgument(to != null, "location");
        Preconditions.checkArgument(to.getWorld() != null, "location.world");
        to.checkFinite();
        net.minecraft.server.v1_12_R1.EntityPlayer handle = ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer) player).getHandle();
        if (player.getHealth() == 0.0D || handle.dead) return false;
        if (handle.playerConnection == null) return false;
        if (handle.isVehicle()) return false;
        Location from = player.getLocation();
        // PlayerTeleportEvent event = new PlayerTeleportEvent(player, from, to, cause);
        // player.getServer().getPluginManager().callEvent(event);
        // if (event.isCancelled()) return false;
        // from = event.getFrom();
        // to = event.getTo();
        handle.stopRiding();
        net.minecraft.server.v1_12_R1.WorldServer fromWorld = ((org.bukkit.craftbukkit.v1_12_R1.CraftWorld) from.getWorld()).getHandle();
        net.minecraft.server.v1_12_R1.WorldServer toWorld = ((org.bukkit.craftbukkit.v1_12_R1.CraftWorld) to.getWorld()).getHandle();
        if (handle.activeContainer != handle.defaultContainer) {
            handle.closeInventory(InventoryCloseEvent.Reason.TELEPORT);
        }
        if (fromWorld == toWorld) {
            handle.playerConnection.teleport(to);
        } else {
            ((org.bukkit.craftbukkit.v1_12_R1.CraftServer) player.getServer()).getHandle()
                    .moveToWorld(handle, toWorld.dimension, true, to, !toWorld.paperConfig.disableTeleportationSuffocationCheck);
        }
        return true;
    }

    @Nullable
    public static Location findBiome(World world, Biome biome) {
        Map<Biome, Set<Coord>> cache = BiomeFinder.getPlugin().getCacheManager().getCache(world);
        if (cache == null) {
            if (CacheBuilder.isBuildRunning()) return null; // Выполняется перестройка кэша
            BiomeFinder.getPlugin().getCacheManager().setCache(BiomeFinder.getPlugin().getBfConfig().loadBiomeCaches());
            cache = BiomeFinder.getPlugin().getCacheManager().getCache(world);
            if (cache == null) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "silent bcachebuild " + world.getName());
                return null; // Перестройка выполняется асинхронно
            }
        }
        Set<Coord> coords = cache.get(biome);
        if (coords == null || coords.size() == 0)
            throw new IllegalStateException("В мире " + world.getName() + " не обнаружен биом " + biome);
        return Randomizer.getElement(coords).asLocation(world);
    }
}
