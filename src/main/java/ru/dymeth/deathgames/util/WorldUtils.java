package ru.dymeth.deathgames.util;

import org.apache.commons.io.FileUtils;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.PluginClassLoader;
import org.spigotmc.SpigotConfig;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class WorldUtils {
    private static final Map<String, Location> fallbackLocations;

    static {
        fallbackLocations = new HashMap<>();
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            private void on(PlayerSpawnLocationEvent event) {
                Location fallback = fallbackLocations.get(event.getSpawnLocation().getWorld().getName());
                if (fallback == null) return;
                event.setSpawnLocation(fallback);
                Bukkit.getScheduler().runTaskLater(plugin(), () ->
                        event.getPlayer().sendMessage(ChatColor.RED + "Мир, в котором вы зареспаунились, был отключён"),
                        20L);
            }

            @EventHandler(ignoreCancelled = true)
            private void on(PlayerTeleportEvent event) {
                Location fallback = fallbackLocations.get(event.getTo().getWorld().getName());
                if (fallback == null) return;
                if (event.getCause() == PlayerTeleportEvent.TeleportCause.UNKNOWN) return; // При входе на сервер
                event.setTo(fallback);
                event.getPlayer().sendMessage(ChatColor.RED + "Мир, в который вы попытались попасть, был отключён");
            }

            // TODO
            // По неизвестной причине FastAsyncWorldEdit иногда отменяет ChunkUnloadEvent
            // в слушателе com.boydti.fawe.bukkit.v1_12.BukkitQueue_1_12 с приоритетом NORMAL
            @EventHandler(priority = EventPriority.HIGH)
            private void on(ChunkUnloadEvent event) {
                if (!event.isCancelled()) return;
                if (!fallbackLocations.containsKey(event.getWorld().getName())) return;
                event.setCancelled(false);
                event.setSaveChunk(false);
            }
        }, plugin());
    }

    public static void recreateWorld(@Nonnull WorldCreator worldCreator, @Nonnull String archiveName, @Nonnull Location fallbackLocation,
                                     @Nonnull Consumer<World> onComplete, @Nonnull Consumer<Exception> onFail) {
        String worldName = worldCreator.name();
        File worldsDirectory = Bukkit.getWorldContainer();
        if (!archiveName.endsWith(".zip")) {
            onFail.accept(new WorldOperationException("Файл архива мира " + worldName + " должен иметь формать .zip (дано " + archiveName + ")"));
            return;
        }
        if (!new File(worldsDirectory, archiveName).isFile()) {
            onFail.accept(new WorldOperationException("Файл архива " + archiveName + " мира " + worldName + " не найден "));
            return;
        }
        Bukkit.getScheduler().runTask(plugin(), () -> {
            World previousWorld = Bukkit.getWorld(worldName);
            Runnable onPreviousUnloaded =
                    () -> clearData(worldCreator.name(), onFail,
                            () -> createFromArchive(archiveName, worldCreator, onFail, onComplete));
            if (previousWorld == null) onPreviousUnloaded.run();
            else unloadWorldFully(previousWorld, fallbackLocation, onFail, onPreviousUnloaded);
        });
    }

    private static void unloadWorldFully(@Nullable World world, @Nonnull Location fallbackLocation,
                                         @Nonnull Consumer<Exception> onFail, @Nonnull Runnable onComplete) {
        if (world == null) {
            onComplete.run();
            return;
        }
        String worldName = world.getName();
        fallbackLocations.put(worldName, fallbackLocation);
        emptyFromPlayers(world, fallbackLocation, onFail,
                () -> unloadChunks(world, onFail,
                        () -> unloadFromBukkit(world, onFail, () -> {
                                    onComplete.run();
                                    fallbackLocations.remove(worldName);
                                }
                        )));
    }

    private static void emptyFromPlayers(@Nullable World world, @Nonnull Location targetLocation,
                                         @Nonnull Consumer<Exception> onFail, @Nonnull Runnable onComplete) {
        if (world == null) {
            onComplete.run();
            return;
        }
        List<Player> players = world.getPlayers();
        if (players.size() < 1) {
            onComplete.run();
            return;
        }
        if (world == targetLocation.getWorld()) {
            onFail.accept(new WorldOperationException("Игроки из освобождаемого мира " + world.getName() + " не могут быть перенесены в этот же мир"));
            return;
        }
        for (Player player : players) {
            boolean vehicle = false;
            if (!player.isEmpty()) {
                for (Entity passenger : player.getPassengers()) {
                    passenger.leaveVehicle();
                }
                vehicle = true;
            }
            boolean dead = false;
            if (player.isDead()) {
                player.spigot().respawn();
                dead = true;
            }
            LocationUtils.teleportWithoutEvent(player, targetLocation);
            if (player.teleport(targetLocation)) continue;
            // Игрок умер, содержит на себе пассажиров, или PlayerTeleportEvent был отменён
            net.minecraft.server.v1_12_R1.Entity nms = ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity) player).getHandle();
            onFail.accept(new WorldOperationException("Ядро отклонило телепортацию игрока " + player.getName() + " из мира " + world.getName() + ". "
                    + "vehicle_bukkit=" + vehicle
                    + "; vehicle_nms=" + vehicle
                    + "; dead_bukkit=" + dead
                    + "; dead_nms=" + dead));
            return;
        }
        Bukkit.getScheduler().runTaskLater(plugin(), onComplete, 40);
    }

    private static void unloadChunks(@Nullable World world,
                                     @Nonnull Consumer<Exception> onFail, @Nonnull Runnable onComplete) {
        if (world == null) {
            onComplete.run();
            return;
        }
        List<Player> players = world.getPlayers();
        if (players.size() > 0) {
            onFail.accept(new WorldOperationException("Мир " + world.getName() + " содержит игроков (" + players.size() + "), отгрузка чанков невозможна: " + playersToString(players)));
            return;
        }
        Chunk[] chunks = world.getLoadedChunks();
        if (chunks.length == 0) {
            onComplete.run();
            return;
        }
        world.setKeepSpawnInMemory(false);
        for (Chunk chunk : chunks) {
            if (chunk.unload(false)) continue; // save
            // В чанке содержатся игроки или был отменён ChunkUnloadEvent
            // onFail.accept(new WorldOperationException("Ядро отклонило выгрузку чанка " + chunk.getX() + ";" + chunk.getZ() + " мира " + world.getName()));
            onFail.accept(new WorldOperationException("Ядро отклонило выгрузку чанка " + chunk.getX() + ";" + chunk.getZ() + " мира " + world.getName() + ". "
                    + "use=" + world.isChunkInUse(chunk.getX(), chunk.getZ())));
            return;
        }
        Bukkit.getScheduler().runTaskLater(plugin(), onComplete, 20 * 3);
    }

    private static void unloadFromBukkit(@Nullable World world,
                                         @Nonnull Consumer<Exception> onFail, @Nonnull Runnable onComplete) {
        if (world == null) {
            onComplete.run();
            return;
        }
        if (world.getLoadedChunks().length > 0) {
            onFail.accept(new WorldOperationException("Чанки мира " + world.getName() + " не отгружены"));
            return;
        }
        try {
            if (!Bukkit.unloadWorld(world, false)) {
                // Мир уже отгружен, мир является дефолтным, содержит игроков, или WorldUnloadEvent был отменён
                onFail.accept(new WorldOperationException("Ядро отклонило выгрузку мира " + world.getName()));
                return;
            }
        } catch (Exception e) {
            onFail.accept(new WorldOperationException("Исключение при выгрузке мира " + world.getName(), e));
            return;
        }
        Bukkit.getScheduler().runTaskLater(plugin(), onComplete, 20);
    }

    private static void clearData(@Nonnull String worldName,
                                  @Nonnull Consumer<Exception> onFail, @Nonnull Runnable onComplete) {
        World previousWorld = Bukkit.getWorld(worldName);
        if (previousWorld != null) {
            onFail.accept(new WorldOperationException("Мир " + previousWorld.getName() + " не выгружен"));
            return;
        }
        File worldDirectory = new File(Bukkit.getWorldContainer(), worldName);
        try {
            if (worldDirectory.isDirectory()) {
                FileUtils.deleteDirectory(worldDirectory);
                if (worldDirectory.isDirectory()) {
                    onFail.accept(new WorldOperationException("Не удалось удалить директорию мира " + worldName));
                    return;
                }
            }
            onComplete.run();
        } catch (Exception e) {
            onFail.accept(new WorldOperationException("Исключение при удалении директории мира " + worldName, e));
        }
    }

    private static void createFromArchive(@Nonnull String archiveName, @Nonnull WorldCreator worldCreator,
                                          @Nonnull Consumer<Exception> onFail, @Nonnull Consumer<World> onComplete) {
        String worldName = worldCreator.name();
        World previousWorld = Bukkit.getWorld(worldName);
        if (previousWorld != null) {
            onFail.accept(new WorldOperationException("Мир " + previousWorld.getName() + " уже создан"));
            return;
        }
        File worldsDirectory = Bukkit.getWorldContainer();
        try {
            ZipUtil.unzip(worldsDirectory.getAbsolutePath(), archiveName);
            // TODO Переименовать директорию из архива в worldName
        } catch (Exception e) {
            onFail.accept(new WorldOperationException("Исключение при разархиваии файла " + archiveName + " мира " + worldName, e));
            return;
        }
        if (!new File(worldsDirectory, worldName).isDirectory()) {
            onFail.accept(new WorldOperationException("Не найдена директория мира " + worldName + " после разархивации " + archiveName));
            return;
        }
        World actualWorld;
        boolean verbose = SpigotConfig.config.getBoolean( "world-settings.default.verbose", true);
        SpigotConfig.config.set( "world-settings.default.verbose", false);
        try {
            // TODO Загрузить Environment мира из его данных и назначить этот Environment worldCreator'у
            actualWorld = Bukkit.createWorld(worldCreator);
        } catch (Exception e) {
            onFail.accept(new WorldOperationException("Исключение при создании мира " + worldName + " из архива " + archiveName, e));
            return;
        } finally {
            SpigotConfig.config.set( "world-settings.default.verbose", verbose);
        }
        try {
            onComplete.accept(actualWorld);
        } catch (Exception e) {
            onFail.accept(new WorldOperationException("Исключение при выполнении действий после создания мира " + worldName, e));
        }
    }

    private static class WorldOperationException extends RuntimeException {
        public WorldOperationException(String message) {
            super(message);
        }

        public WorldOperationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static String playersToString(List<Player> players) {
        return players.stream().map(HumanEntity::getName).collect(Collectors.joining(";"));
    }

    private static Plugin plugin() {
        return ((PluginClassLoader) WorldUtils.class.getClassLoader()).getPlugin();
    }
}
