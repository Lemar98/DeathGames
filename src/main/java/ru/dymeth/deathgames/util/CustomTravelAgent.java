package ru.dymeth.deathgames.util;

import net.minecraft.server.v1_12_R1.BlockPosition;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;

import javax.annotation.Nullable;

public class CustomTravelAgent {
    @Nullable
    public static Location findOrCreate(Location target) {
        Location found = findPortal(target);
        if (found != null) return found;

        if (!((CraftWorld) target.getWorld()).getHandle().getTravelAgent()
                .createPortal(target.getX(), target.getY(), target.getZ(), 16)) return null;

        return findPortal(target);
    }

    @Nullable
    private static Location findPortal(Location location) {
        BlockPosition found = ((CraftWorld) location.getWorld()).getHandle()
                .getTravelAgent().findPortal(location.getX(), location.getY(), location.getZ(), 128);
        if (found == null) return null;
        return new Location(location.getWorld(),
                found.getX(), found.getY(), found.getZ(),
                location.getYaw(), location.getPitch());
    }
}
