package ru.dymeth.deathgames.target;

import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import ru.dymeth.deathgames.item.ItemBuilder;
import ru.dymeth.deathgames.listener.CustomListener;
import ru.dymeth.deathgames.util.Randomizer;
import ru.dymeth.deathgames.util.RegularMob;

import java.util.*;
import java.util.function.Predicate;

import static ru.dymeth.deathgames.util.EntityUtils.getSourceDamager;

@SuppressWarnings("unused")
public class GameTarget {
    private static final List<GameTarget> allValues = new ArrayList<>();

    public static GameTarget CONTACT = new GameTargetBuilder(
            5 * 60, true,
            "коснуться кактуса", "уберечь %player% от кактусов")
            .filter(EntityDamageEvent.DamageCause.CONTACT)
            .addToVictimKit(5, ItemBuilder.newBuilder(Material.CACTUS).setAmount(2))
            .setAllowedBiomes(Biome.DESERT, Biome.DESERT_HILLS, Biome.MUTATED_DESERT)
            .build();

    public static GameTarget ZOMBIE_ATTACK = new GameTargetBuilder(
            7 * 60, true,
            "умереть от зомби или кадавра", "спасти %player% от зомби и кадавров")
            .filter(event -> {
                switch (event.getCause()) {
                    case ENTITY_ATTACK:
                    case ENTITY_SWEEP_ATTACK:
                        // Вернёт true для зомби, зомби-жителя и кадавра
                        return ((EntityDamageByEntityEvent) event).getDamager() instanceof Zombie;
                    default:
                        return false;
                }
            })
            .addRegularMob(Zombie.class, 10, 60, "Зомби заспаунился")
            .night()
            .build();

    public static GameTarget SPIDER_ATTACK = new GameTargetBuilder(
            7 * 60, true,
            "умереть от паука", "спасти %player% от пауков")
            .filter(event -> {
                switch (event.getCause()) {
                    case ENTITY_ATTACK:
                    case ENTITY_SWEEP_ATTACK:
                        // Вернёт true для паука и пещерного паука
                        return ((EntityDamageByEntityEvent) event).getDamager() instanceof Spider;
                    default:
                        return false;
                }
            })
            .addRegularMob(Spider.class, 10, 60, "Паук заспаунился")
            .night()
            .build();

    public static GameTarget SKELETON_ATTACK = new GameTargetBuilder(
            7 * 60, true,
            "умереть от скелета или зимогора", "спасти %player% от скелетов и зимогоров")
            .filter(event -> {
                if (event.getCause() != EntityDamageEvent.DamageCause.PROJECTILE) return false;
                // Вернёт true для скелета, зимогора и скелета-иссушителя
                return getSourceDamager(event) instanceof Skeleton;
            })
            .addRegularMob(Skeleton.class, 10, 60, "Скелет заспаунился")
            .night()
            .build();

    public static GameTarget SUFFOCATION = new GameTargetBuilder(
            7 * 60, true,
            "задохнуться от гравия или песка", "уберечь %player% от застревания в сыпучих блоках")
            .filter(EntityDamageEvent.DamageCause.SUFFOCATION)
            .build();

    public static GameTarget FALL = new GameTargetBuilder(
            5 * 60, true,
            "упасть с большой высоты", "спасти %player% от падения с большой высоты")
            .filter(EntityDamageEvent.DamageCause.FALL)
            .addToOpponentsKit(30, ItemBuilder.newBuilder(Material.WATER_BUCKET))
            .addToOpponentsKit(30, ItemBuilder.newBuilder(Material.WEB).setAmount(2))
            .addToOpponentsKit(5, ItemBuilder.newBuilder(Material.SLIME_BLOCK).setAmount(5))
            .setListener(new CustomListener() {
                @EventHandler
                private void on(PlayerBucketEmptyEvent event) {
                    Bukkit.getScheduler().runTask(this.getPlugin(), () ->
                            event.getPlayer().getInventory().setItemInMainHand(null));
                }
            })
            .build();

    public static GameTarget FIRE = new GameTargetBuilder(
            5 * 60, false,
            "спастись от огня и лавы", "поджарить %player% огнём или лавой")
            .filter(EntityDamageEvent.DamageCause.FIRE, EntityDamageEvent.DamageCause.FIRE_TICK, EntityDamageEvent.DamageCause.LAVA)
            .addToOpponentsKit(10, ItemBuilder.newBuilder(Material.LAVA_BUCKET))
            .addToOpponentsStartKit(ItemBuilder.newBuilder(Material.FLINT_AND_STEEL))
            .addToVictimStartKit(ItemBuilder.newBuilder(Material.WATER_BUCKET))
            .build();

    public static GameTarget DROWNING = new GameTargetBuilder(
            5 * 60, true,
            "утонуть в воде", "спасти %player% от утопления")
            .filter(EntityDamageEvent.DamageCause.DROWNING)
            .addToOpponentsStartKit(ItemBuilder.newBuilder(Material.BUCKET))
            .addToOpponentsKit(60, ItemBuilder.newBuilder(Material.SPONGE).setAmount(5))
            .build();

    public static GameTarget EXPLOSION = new GameTargetBuilder(
            5 * 60, false,
            "спастись от взрывов", "взорвать %player%")
            .filter(event -> {
                switch (event.getCause()) {
                    case BLOCK_EXPLOSION:
                    case ENTITY_EXPLOSION:
                        return true;
                    case PROJECTILE:
                        return getSourceDamager(event) instanceof Ghast;
                    default:
                        return false;
                }
            })
            .night()
            .addToVictimStartKit(ItemBuilder.newBuilder(Material.BOW).addEnchantment(Enchantment.ARROW_INFINITE, 1).setUnbreakable())
            .addToVictimStartKit(ItemBuilder.newBuilder(Material.ARROW))
            .addToVictimStartKit(ItemBuilder.newBuilder(Material.WATER_BUCKET))
            .addToVictimStartKit(ItemBuilder.newBuilder(Material.IRON_SWORD).setUnbreakable())
            .addToOpponentsKit(30, ItemBuilder.newBuilder(Material.FLINT_AND_STEEL))
            .addToOpponentsKit(15, ItemBuilder.newBuilder(Material.TNT).setAmount(2))
            .addToOpponentsKit(15, ItemBuilder.newBuilder(Material.SPONGE).setAmount(2))
            .addToVictimKit(30, ItemBuilder.newBuilder(Material.WATER_BUCKET))
            .setListener(new CustomListener() {
                @EventHandler
                private void on(PlayerBucketEmptyEvent event) {
                    Bukkit.getScheduler().runTask(this.getPlugin(), () ->
                            event.getPlayer().getInventory().setItemInMainHand(null));
                }

                @EventHandler
                private void on(EntityDamageEvent event) {
                    switch (event.getCause()) {
                        case FIRE:
                        case FIRE_TICK:
                            break;
                        default:
                            return;
                    }
                    event.setCancelled(true);
                }
            })
            .addRegularMob(Ghast.class, 10, 60, "Гаст заспаунился", arena -> {
                Location location = arena.getVictim().getLocation();
                location.setY(location.getY() + 10);
                if (location.getBlock().getType() != Material.AIR) return null;
                return location;
            })
            .build();

    public static GameTarget LIGHTNING = new GameTargetBuilder(
            5 * 60, false,
            "спастись от молний", "поджарить %player% молнией")
            .filter(EntityDamageEvent.DamageCause.LIGHTNING)
            .addToOpponentsKit(7,
                    ItemBuilder.newBuilder(Material.DIAMOND_AXE)
                            .setID("thunder_item")
                            .setName(ChatColor.RED + "" + ChatColor.BOLD + "Молот Тора")
                            .addLore(ChatColor.YELLOW + "Выбери место, и туда ударит молния!")
                            .addLore(ChatColor.YELLOW + "Дистанция: 7 блоков")
                            .addEnchantment(Enchantment.LUCK, 1)
                            .addFlag(ItemFlag.HIDE_ENCHANTS)
            )
            .night()
            .thunder()
            .build();

    public static GameTarget MAGIC = new GameTargetBuilder(
            5 * 60, false,
            "спастись от зелий вреда", "закидать %player% зельями вреда")
            .filter(EntityDamageEvent.DamageCause.MAGIC)
            .addToOpponentsKit(7, ItemBuilder.newBuilder(Material.SPLASH_POTION).setPotionData(PotionType.INSTANT_DAMAGE))
            .build();

    public static GameTarget WITHER = new GameTargetBuilder(
            5 * 60, true,
            "умереть от иссушения", "спасти %player% от иссушения")
            .filter(EntityDamageEvent.DamageCause.WITHER)
            .setListener(new CustomListener() {
                @EventHandler
                private void on(EntityDamageEvent event) {
                    if (!(event.getEntity() instanceof Player)) return;
                    switch (event.getCause()) {
                        case ENTITY_ATTACK:
                        case ENTITY_EXPLOSION:
                        case PROJECTILE: {
                            if (!(getSourceDamager(event) instanceof Wither)) return;
                            event.setCancelled(true);

                            World world = event.getEntity().getWorld();
                            byte duration = 0;
                            if (world.getDifficulty() == Difficulty.PEACEFUL) duration = 0;
                            else if (world.getDifficulty() == Difficulty.EASY) duration = 5;
                            else if (world.getDifficulty() == Difficulty.NORMAL) duration = 10;
                            else if (world.getDifficulty() == Difficulty.HARD) duration = 40;
                            if (duration > 0)
                                ((Player) event.getEntity()).addPotionEffect(
                                        new PotionEffect(PotionEffectType.WITHER, 20 * duration, 1));
                        }
                    }
                }
            })
            .addRegularMob(Wither.class, 5, 30, "Иссушитель заспаунился", arena -> {
                Location location = arena.getVictim().getLocation();
                location.setY(location.getY() + 10);
                if (location.getNearbyEntitiesByType(Wither.class, 30f).size() > 0) return null;
                return location;
            })
            .build();

    public static GameTarget FALLING_BLOCK = new GameTargetBuilder(
            5 * 60, true,
            "умереть от падения наковальни на голову", "спасти %player% от падения наковальни на голову")
            .filter(EntityDamageEvent.DamageCause.FALLING_BLOCK)
            .addToVictimKit(30, ItemBuilder.newBuilder(Material.ANVIL))
            .build();

    public static GameTarget DRAGON_BREATH = new GameTargetBuilder(
            5 * 60, true,
            "умереть от драконьего дыхания", "спасти %player% от драконьего дыхания")
            .filter(event -> {
                switch (event.getCause()) {
                    case DRAGON_BREATH:
                        return true;
                    case ENTITY_ATTACK:
                        return getSourceDamager(event) instanceof AreaEffectCloud;
                }
                return false;
            })
            .world("arena_the_end")
            .addToOpponentsStartKit(ItemBuilder.newBuilder(Material.STICK).addEnchantment(Enchantment.KNOCKBACK, 2))
            .setListener(new CustomListener() {
                @EventHandler
                private void on(EntityDamageEvent event) {
                    switch (event.getEntity().getType()) {
                        case ENDER_DRAGON:
                        case ENDER_CRYSTAL:
                            event.setCancelled(true);
                    }
                    if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
                        event.setCancelled(true);
                    }
                }

                @EventHandler
                private void on(EntityTargetEvent event) {
                    if (event.getEntity().getType() == EntityType.ENDER_DRAGON) return;
                    if (event.getTarget() == null) return;
                    event.setCancelled(true);
                }
            })
            .build();

    public static List<GameTarget> values() {
        return Collections.unmodifiableList(allValues);
    }

    private final int durationSeconds; // Продолжительность игры на арене в секундах
    private final boolean shouldDie; // Если true - задача умереть, если false - спастись
    private final String victimMessage;
    private final String opponentsMessage;
    private final Predicate<EntityDamageEvent> filter;
    private final String world;
    private final boolean night; // Если true - будет установлена ночь, если false - день
    private final boolean thunder; // Если true - будет установлена грозовая погода, если false - ясная

    private final Map<ItemStack, Integer> victimKitCooldowns;
    private final Map<ItemStack, Integer> opponentsKitCooldowns;
    private final Listener listener;
    private final List<Biome> allowedBiomes;
    private final Set<RegularMob> regularMobs;

    GameTarget(String world,
               boolean night, boolean thunder,
               boolean shouldDie, int durationSeconds,
               String victimMessage, String opponentsMessage,
               Predicate<EntityDamageEvent> filter,
               Map<ItemStack, Integer> victimKitCooldowns, Map<ItemStack, Integer> opponentsKitCooldowns,
               Listener listener,
               List<Biome> allowedBiomes,
               Set<RegularMob> regularMobs) {
        this.world = world;
        this.night = night;
        this.thunder = thunder;
        this.shouldDie = shouldDie;
        this.durationSeconds = durationSeconds;
        this.filter = filter;
        this.victimMessage = victimMessage;
        this.opponentsMessage = opponentsMessage;
        this.victimKitCooldowns = new HashMap<>(victimKitCooldowns);
        this.opponentsKitCooldowns = new HashMap<>(opponentsKitCooldowns);
        this.listener = listener;
        this.allowedBiomes = allowedBiomes;
        this.regularMobs = regularMobs;
        allValues.add(this);
    }

    public int getDurationSeconds() {
        return this.durationSeconds;
    }

    public boolean isShouldDie() {
        return this.shouldDie;
    }

    public String getVictimMessage() {
        return this.victimMessage;
    }

    public String getOpponentsMessage(ChatColor nameColor, Player victim, ChatColor messageColor) {
        return this.opponentsMessage.replace("%player%", nameColor + victim.getName() + messageColor);
    }

    public boolean isCauseOf(Player player) {
        return this.filter.test(player.getLastDamageCause());
    }

    public String getWorldName() {
        return this.world;
    }

    public boolean isNightRequired() {
        return this.night;
    }

    public boolean isThunderRequired() {
        return this.thunder;
    }

    public Map<ItemStack, Integer> getKitCooldowns(boolean victim) {
        return victim ? this.victimKitCooldowns : this.opponentsKitCooldowns;
    }

    public Listener getListener() {
        return this.listener;
    }

    public Biome getRandomAllowedBiome() {
        if (this.allowedBiomes == null) return null;
        return Randomizer.getElement(this.allowedBiomes);
    }

    public Map<RegularMob, Integer> createMobCooldowns() {
        Map<RegularMob, Integer> result = new HashMap<>();
        for (RegularMob mob : this.regularMobs)
            result.put(mob, mob.getFirstCooldownSeconds());
        return result;
    }
}
