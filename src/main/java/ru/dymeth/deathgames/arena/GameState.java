package ru.dymeth.deathgames.arena;

public enum GameState {
    START,
    GAME,
    END
}
