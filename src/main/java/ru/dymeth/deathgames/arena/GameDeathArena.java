package ru.dymeth.deathgames.arena;

import com.destroystokyo.paper.entity.SentientNPC;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitTask;
import ru.dymeth.deathgames.DeathLogger;
import ru.dymeth.deathgames.economy.EconomyManager;
import ru.dymeth.deathgames.item.ItemBuilder;
import ru.dymeth.deathgames.target.GameTarget;
import ru.dymeth.deathgames.util.*;

import java.util.*;
import java.util.stream.Collectors;

public final class GameDeathArena extends GameArena {
    private final World lobbyWorld;
    private World arenaWorld;

    private GameTarget target;
    private BossBar victimBar;
    private BossBar opponentsBar;
    private BukkitTask compassUpdateTask;
    private final EconomyManager economyManager;
    private Player victim;
    private Set<Player> opponents;
    private int middleX = 0;
    private int middleY = 0;
    private int middleZ = 0;
    private final Map<ItemStack, Integer> victimKitCooldowns;
    private final Map<ItemStack, Integer> opponentsKitCooldowns;
    private Listener listener;
    private final Map<RegularMob, Integer> mobCooldowns;

    public GameDeathArena(Plugin plugin, EconomyManager economyManager,
                          int minStartPlayers, int minPlayersTimerSeconds,
                          int maxStartPlayers, int maxPlayersTimerSeconds,
                          int endingDurationSeconds) {
        super(plugin,
                minStartPlayers, minPlayersTimerSeconds,
                maxStartPlayers, maxPlayersTimerSeconds,
                endingDurationSeconds);

        this.economyManager = economyManager;
        this.lobbyWorld = Bukkit.getWorlds().iterator().next();
        this.lobbyWorld.setGameRuleValue("doDaylightCycle", String.valueOf(false));
        this.lobbyWorld.setGameRuleValue("doWeatherCycle", String.valueOf(false));
        this.lobbyWorld.setPVP(false);
        this.lobbyWorld.setTime(18_000);
        this.lobbyWorld.setThundering(false);
        this.lobbyWorld.setStorm(false);
        this.victimKitCooldowns = new HashMap<>();
        this.opponentsKitCooldowns = new HashMap<>();
        this.mobCooldowns = new HashMap<>();
        this.restartGame();
    }

    private Map<ItemStack, Integer> filterPositiveCooldowns(Map<ItemStack, Integer> source) {
        return source.entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 0)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue));
    }

    @Override
    public void onSecondTick() {
        this.giveKitOnCooldown(true);
        this.giveKitOnCooldown(false);
        this.spawnRegularMobs();
    }

    private void giveKitOnCooldown(boolean victim) {
        Set<Player> targets = victim ? Collections.singleton(this.victim) : this.opponents;
        Map<ItemStack, Integer> map = this.getKitCooldowns(victim, true);
        for (Map.Entry<ItemStack, Integer> entry : new HashMap<>(map).entrySet()) {
            ItemStack stack = entry.getKey();
            int cooldown = entry.getValue() - 1;
            if (cooldown <= 0) {
                Integer tempCooldown = this.getKitCooldowns(victim, false).get(stack);
                if (tempCooldown == null) {
                    for (Map.Entry<ItemStack, Integer> stack1 : this.getKitCooldowns(victim, false).entrySet()) {
                        if (!stack.isSimilar(stack1.getKey())) continue;
                        tempCooldown = stack1.getValue();
                        break;
                    }
                    if (tempCooldown == null) {
                        this.plugin.getLogger().severe("Не удалось получить кулдаун предмета " + stack);
                        continue;
                    }
                    this.plugin.getLogger().warning("Кулдаун предмета " + stack + "получен перебором");
                }
                cooldown = tempCooldown;
                String itemName;
                if (stack.hasItemMeta() && stack.getItemMeta().hasDisplayName())
                    itemName = stack.getItemMeta().getDisplayName();
                else
                    itemName = stack.getType().name().replace("_", " ").toLowerCase();
                String msg = "§eВы получите §6" + itemName + "§e ещё раз через §6" + cooldown + " сек.";
                int requiredAmount = stack.getAmount();
                playerCycle:
                for (Player target : targets) {
                    int currentAmount = 0;
                    for (ItemStack stack1 : target.getInventory().all(stack.getType()).values()) {
                        if (!stack.isSimilar(stack1)) continue;
                        currentAmount += stack1.getAmount();
                        if (currentAmount >= requiredAmount) continue playerCycle;
                    }
                    ItemStack give = stack.clone();
                    give.setAmount(requiredAmount - currentAmount);
                    target.getInventory().addItem(give);
                    target.sendMessage(msg);
                }
            }
            map.put(stack, cooldown);
        }
    }

    private void spawnRegularMobs() {
        for (Map.Entry<RegularMob, Integer> entry : new HashMap<>(this.mobCooldowns).entrySet()) {
            RegularMob regularMob = entry.getKey();
            int cooldown = entry.getValue() - 1;
            if (cooldown <= 0) {
                cooldown = regularMob.getCooldownSeconds();
                Location location = regularMob.getSpawnLocation(this);
                if (location != null) {
                    Entity entity = location.getWorld().spawn(location, regularMob.getEntityClass());
                    if (entity instanceof SentientNPC) ((SentientNPC) entity).setTarget(this.victim);
                    this.broadcastMessage(ChatColor.RED + regularMob.getSpawnMessage() + "! До следующего спауна " + cooldown + " сек.");
                }
            }
            this.mobCooldowns.put(regularMob, cooldown);
        }
    }

    @Override
    protected void prepareArena() {
        this.target = Randomizer.getElement(GameTarget.values());
        this.victimKitCooldowns.putAll(filterPositiveCooldowns(this.getKitCooldowns(true, false)));
        this.opponentsKitCooldowns.putAll(filterPositiveCooldowns(this.getKitCooldowns(false, false)));
        this.mobCooldowns.putAll(this.target.createMobCooldowns());
        this.listener = this.target.getListener();
        String worldName = this.target.getWorldName();
        World.Environment environment;
        try {
            environment = World.Environment.valueOf(worldName.substring(worldName.indexOf("_") + 1).toUpperCase());
        } catch (Exception e) {
            throw new IllegalArgumentException("Не удалось определить Environment по его названию: " + worldName);
        }
        WorldUtils.recreateWorld(
                new WorldCreator(worldName).environment(environment),
                worldName + ".zip",
                this.lobbyWorld.getSpawnLocation(),
                this::prepareArena,
                e -> {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "plugman reload " + this.plugin.getName());
                    throw new RuntimeException("Не удалось создать мир арены", e);
                });
    }

    private void prepareArena(World world) {
        this.arenaWorld = world;
        this.arenaWorld.setGameRuleValue("doDaylightCycle", String.valueOf(false));
        this.arenaWorld.setGameRuleValue("doWeatherCycle", String.valueOf(false));
        this.arenaWorld.setGameRuleValue("announceAdvancements", String.valueOf(false));
        this.arenaWorld.setPVP(true);
        this.arenaWorld.setDifficulty(Difficulty.HARD);

        this.arenaWorld.setTime(this.target.isNightRequired() ? 18_000 : 6_000);
        this.arenaWorld.setStorm(this.target.isThunderRequired());
        this.arenaWorld.setThundering(this.target.isThunderRequired());

        World.Environment environment = this.arenaWorld.getEnvironment();
        switch (environment) {
            case NORMAL:
                while (true) {
                    Biome biome = this.target.getRandomAllowedBiome();
                    if (biome == null) {
                        int middleX = Randomizer.getInt(-2_500, 2_500);
                        int middleZ = Randomizer.getInt(-2_500, 2_500);
                        switch (this.arenaWorld.getBiome(middleX, middleZ)) {
                            case OCEAN:
                            case RIVER:
                            case MUSHROOM_ISLAND:
                            case MUSHROOM_ISLAND_SHORE:
                            case DEEP_OCEAN:
                            case MESA:
                            case MESA_ROCK:
                            case MESA_CLEAR_ROCK:
                            case MUTATED_MESA:
                            case MUTATED_MESA_ROCK:
                            case MUTATED_MESA_CLEAR_ROCK:
                                continue;
                        }
                        this.setMiddle(middleX, middleZ);
                    } else {
                        Location findLocation = LocationUtils.findBiome(this.arenaWorld, biome);
                        if (findLocation == null) break;
                        this.setMiddle(findLocation.getBlockX(), findLocation.getBlockZ());
                    }
                    break;
                }
                break;
            case NETHER: {
                int x = Randomizer.getInt(-1_500, 1_500);
                int z = Randomizer.getInt(-1_500, 1_500);
                Location netherLocation = new Location(this.arenaWorld, x, 80.0, z);
                Location newNetherLocation = CustomTravelAgent.findOrCreate(netherLocation);
                if (newNetherLocation == null)
                    throw new IllegalStateException("Не удалось создать портал в мире " + this.arenaWorld.getName());
                this.middleY = newNetherLocation.getBlockY();
                this.setMiddle(newNetherLocation.getBlockX(), newNetherLocation.getBlockZ());
            }
            break;
            case THE_END:
                Location spawn = this.arenaWorld.getSpawnLocation();
                this.setMiddle(spawn.getBlockX(), spawn.getBlockZ());
                break;
        }
    }

    public void findMiddleOf(World world) {
        if (this.arenaWorld != world) return;
        Biome biome = this.target.getRandomAllowedBiome();
        if (biome == null) return;
        Location findLocation = LocationUtils.findBiome(this.arenaWorld, biome);
        if (findLocation == null)
            throw new IllegalArgumentException("Не удалось найти точку спауна в мире " + this.arenaWorld.getName() + " (биом " + biome + ")");
        this.setMiddle(findLocation.getBlockX(), findLocation.getBlockZ());
    }

    public void setMiddle(int middleX, int middleZ) {
        this.middleX = middleX;
        this.middleZ = middleZ;
        this.plugin.getLogger().info("Поиск точки спауна завершён");
    }

    @Override
    protected void onGameStarted() {
        if (this.arenaWorld == null) throw new IllegalArgumentException("Не обнаружен мир арены");

        //this.victim = Randomizer.getElement(this.arenaPlayers);
        this.victim = getRandomVictim();
        this.victim.setGlowing(true);
        this.opponents = new HashSet<>(this.arenaPlayers);
        this.opponents.remove(this.victim);

        this.victimBar = Bukkit.createBossBar(
                ChatColor.YELLOW + "Ваша задача " + this.target.getVictimMessage(),
                BarColor.YELLOW, BarStyle.SEGMENTED_6);
        this.victimBar.addPlayer(this.victim);
        this.opponentsBar = Bukkit.createBossBar(
                ChatColor.YELLOW + "Вам необходимо " + this.target.getOpponentsMessage(ChatColor.GOLD, this.victim, ChatColor.YELLOW),
                BarColor.YELLOW, BarStyle.SEGMENTED_6);
        this.opponents.forEach(player -> this.opponentsBar.addPlayer(player));

        DeathLogger.info(ChatColor.GREEN + "Игра началась! Жертве необходимо " + this.target.getVictimMessage());

        if (this.arenaWorld.getEnvironment() == World.Environment.NORMAL) {
            ItemStack compass = ItemBuilder.newBuilder(Material.COMPASS)
                    .setName(ChatColor.GOLD + "Детектор игрока")
                    .addLore(ChatColor.GREEN + "Указывает на игрока цель")
                    .create();
            for (Player player : this.opponents) {
                player.getInventory().addItem(compass);
            }
            this.compassUpdateTask = Bukkit.getScheduler().runTaskTimer(this.plugin, () -> {
                Location location = this.victim.getLocation();
                for (Player player : this.opponents)
                    player.setCompassTarget(location);
            }, 20L, 20L);
        }

        for (Player player : this.arenaPlayers) {
            player.teleport(this.getSpawnLocation());

            for (ItemStack item : this.getKitCooldowns(player == this.victim, false).keySet())
                player.getInventory().addItem(item.clone());

            player.sendMessage(ChatColor.GREEN + "Игра началась!");
            this.sendTargetMessage(player);
        }
        if (this.listener != null) Bukkit.getPluginManager().registerEvents(this.listener, this.plugin);
    }

    @Override
    protected int getGameDurationSeconds() {
        return this.target.getDurationSeconds();
    }

    @Override
    protected void endGameByTimedOut() {
        this.endGame(!this.target.isShouldDie());
    }

    @Override
    protected void endGameByPlayerLeftIfRequired(Player player) {
        if (player == this.victim) {
            this.endGame(false);
        } else if (this.opponents.size() < 1) {
            this.endGame(true);
        }
    }

    @Override
    public void forceUnload() {
        if (this.victimBar != null) {
            this.victimBar.removePlayer(this.victim);
            for (Player player : this.opponents)
                this.opponentsBar.removePlayer(player);
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getWorld() != this.lobbyWorld)
                player.teleport(this.lobbyWorld.getSpawnLocation());
            if (player.isGlowing())
                player.setGlowing(false);
        }
        this.broadcastMessage(ChatColor.RED + "Арена была отключена по техническим причинам");
    }

    public void endGame(boolean victimWin) {
        this.victimBar.removePlayer(this.victim);
        this.victimBar = null;
        for (Player player : this.opponents)
            this.opponentsBar.removePlayer(player);
        this.opponentsBar = null;
        if (this.compassUpdateTask != null) {
            this.compassUpdateTask.cancel();
            this.compassUpdateTask = null;
        }
        this.arenaWorld.setThundering(false);
        this.arenaWorld.setStorm(false);
        this.arenaWorld = null;
        this.victimKitCooldowns.clear();
        this.opponentsKitCooldowns.clear();
        this.mobCooldowns.clear();

        String message;
        if (victimWin)
            message = ChatColor.GREEN + "Игра завершилась победой жертвы!";
        else
            message = ChatColor.GREEN + "Игра завершилась победой " + (this.target.isShouldDie() ? "спасателей" : "охотников") + "!";
        this.broadcastMessage(message);
        DeathLogger.info(message + " Продолжительность игры: " + (this.target.getDurationSeconds() - this.phaseSecondsLeft) + " сек");

        if (victimWin)
            this.economyManager.depositPrize(Collections.singleton(this.victim), this.opponents);
        else
            this.economyManager.depositPrize(this.opponents, Collections.singleton(this.victim));

        this.endGame();
    }

    @Override
    protected void onGameEnded() {
        if (this.listener != null) HandlerList.unregisterAll(this.listener);
        this.listener = null;
    }

    public GameTarget getTarget() {
        return this.target;
    }

    public void sendTargetMessage(Player player) {
        if (player == this.victim)
            player.sendMessage(ChatColor.YELLOW + "Ваша задача " + this.target.getVictimMessage());
        else
            player.sendMessage(ChatColor.YELLOW + "Вам необходимо " + this.target.getOpponentsMessage(ChatColor.GOLD, this.victim, ChatColor.YELLOW));
    }

    public Player getVictim() {
        return this.victim;
    }

    public Location getSpawnLocation() {
        if (this.getState() != GameState.GAME) return this.lobbyWorld.getSpawnLocation();
        int randomX;
        int randomZ;
        Location spawnLoc;
        int limit = 0;
        while (true) {
            limit++;
            if (limit == 20) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "plugman reload " + this.plugin.getName());
                throw new IllegalStateException("Не удалось найти подходящее место спаунат возле точки "
                        + this.middleX + ";" + this.middleZ + " в мире " + this.arenaWorld.getName() + " (20 попыток). "
                        + "Перезагружаем плагин...");
            }
            if (this.arenaWorld.getEnvironment().equals(World.Environment.NETHER)) {
                randomX = Randomizer.getInt(-5, 5) + this.middleX;
                randomZ = Randomizer.getInt(-5, 5) + this.middleZ;
                spawnLoc = new Location(this.arenaWorld, randomX + 0.5, 0, randomZ + 0.5);
                spawnLoc.setY(this.middleY);
                Block floorBlock = spawnLoc.getBlock();
                Block footBlock = floorBlock.getRelative(BlockFace.UP);
                Block headBlock = footBlock.getRelative(BlockFace.UP);
                if (floorBlock.getType().equals(Material.AIR)) continue;
                if (!footBlock.getType().equals(Material.AIR)) continue;
                if (!headBlock.getType().equals(Material.AIR)) continue;
                spawnLoc.setY(this.middleY - 2);
            } else {
                randomX = Randomizer.getInt(-10, 10) + this.middleX;
                randomZ = Randomizer.getInt(-10, 10) + this.middleZ;
                spawnLoc = new Location(this.arenaWorld, randomX + 0.5, 0, randomZ + 0.5);
                spawnLoc.getBlock(); // Загрузка чанка с диска
                int highest = LocationUtils.getHighestBlockYAt(spawnLoc, false, true);
                if (highest < 0) continue;
                spawnLoc.setY(highest);
            }
            break;
        }
        spawnLoc.setY(spawnLoc.getY() + 1);
        return spawnLoc;
    }

    public Map<ItemStack, Integer> getKitCooldowns(boolean victim, boolean onlyRegular) {
        if (onlyRegular)
            return victim ? this.victimKitCooldowns : this.opponentsKitCooldowns;
        else
            return fillBasicKit(victim, new HashMap<>(this.getTarget().getKitCooldowns(victim)));
    }

    private Map<ItemStack, Integer> fillBasicKit(boolean victim, Map<ItemStack, Integer> kit) {
        kit.put(ItemBuilder.newBuilder(Material.IRON_AXE).setUnbreakable().create(), -1);
        kit.put(ItemBuilder.newBuilder(Material.IRON_PICKAXE).addEnchantment(Enchantment.DIG_SPEED, 10).setUnbreakable().create(), -1);
        kit.put(ItemBuilder.newBuilder(Material.IRON_SPADE).addEnchantment(Enchantment.DIG_SPEED, 10).setUnbreakable().create(), -1);
        kit.put(ItemBuilder.newBuilder(Material.COOKED_BEEF).setAmount(64).create(), -1);
        if ((victim && !this.target.isShouldDie()) || !victim && this.target.isShouldDie()) {
            kit.put(ItemBuilder.newBuilder(Material.SPLASH_POTION).setPotionData(PotionType.INSTANT_HEAL).create(), -1);
        }
        if (!victim) {
            kit.put(ItemBuilder.newBuilder(Material.WOOL).setColor(DyeColor.RED).setAmount(64).create(), 60);
            kit.put(ItemBuilder.newBuilder(Material.FISHING_ROD).create(), -1);
            kit.put(ItemBuilder.newBuilder(Material.LEATHER_BOOTS).addEnchantment(Enchantment.FROST_WALKER, 1).setUnbreakable().create(), -1);
        }
        kit.put(ItemBuilder.newBuilder(Material.ENDER_PEARL).create(), victim ? 30 : 25);
        return kit;
    }

    private Player getRandomVictim() {
        String perm = "deathgames.forcevictim";
        int adminPlayers = (int) this.arenaPlayers.stream().filter(player -> player.hasPermission(perm)).count();
        int defaultPlayers = this.arenaPlayers.size() - adminPlayers;
        List<Player> victimChance = new ArrayList<>();
        this.arenaPlayers.forEach(player -> {
            if (player.hasPermission(perm)) {
                for (int i = 0; i < defaultPlayers; i++) {
                    victimChance.add(player);
                }
            } else {
                victimChance.add(player);
            }
        });
        Player randomPlayer = Randomizer.getElement(victimChance);
        return randomPlayer != null ? randomPlayer : Randomizer.getElement(this.arenaPlayers);
    }
}
