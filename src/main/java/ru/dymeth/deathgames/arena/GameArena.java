package ru.dymeth.deathgames.arena;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.dymeth.deathgames.event.ArenaEndEvent;
import ru.dymeth.deathgames.event.ArenaEverySecondTickEvent;
import ru.dymeth.deathgames.event.ArenaStartEvent;

import java.util.HashSet;
import java.util.Set;

public abstract class GameArena {
    final Plugin plugin;
    private final int minStartPlayers; // Как только достигаем - до начала игры 2 минуты
    private final int minPlayersTimerSeconds;
    private final int maxStartPlayers; // Как только достигаем - до начала игры 30 секунд
    private final int maxPlayersTimerSeconds;
    protected final int endingDurationSeconds;
    final Set<Player> arenaPlayers = new HashSet<>();
    private GameState state;
    protected int phaseSecondsLeft;

    public GameArena(Plugin plugin,
                     int minStartPlayers, int minPlayersTimerSeconds,
                     int maxStartPlayers, int maxPlayersTimerSeconds,
                     int endingDurationSeconds) {
        this.plugin = plugin;
        this.minStartPlayers = minStartPlayers;
        this.minPlayersTimerSeconds = minPlayersTimerSeconds;
        this.maxPlayersTimerSeconds = maxPlayersTimerSeconds;
        this.maxStartPlayers = maxStartPlayers;
        this.endingDurationSeconds = endingDurationSeconds;
        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            if (this.phaseSecondsLeft < 0) return;

            --this.phaseSecondsLeft;

            if (this.state == GameState.START || this.state == GameState.GAME) {
                boolean printTime = this.phaseSecondsLeft >= 60 && this.phaseSecondsLeft % 60 == 0;
                if (!printTime) {
                    switch (this.phaseSecondsLeft) {
                        case 30:
                        case 20:
                        case 10:
                        case 5:
                        case 4:
                        case 3:
                        case 2:
                        case 1:
                            printTime = true;
                            break;
                    }
                }
                StringBuilder timeMessage = new StringBuilder();
                if (this.phaseSecondsLeft <= 5) timeMessage.append(ChatColor.RED);
                else timeMessage.append(ChatColor.YELLOW);
                timeMessage.append("До ");
                if (this.state == GameState.GAME) timeMessage.append("конца ");
                else timeMessage.append("начала ");
                if (this.phaseSecondsLeft >= 60) {
                    if (this.phaseSecondsLeft % 60 == 0) {
                        timeMessage.append(this.phaseSecondsLeft / 60).append(" минут");
                    } else {
                        timeMessage.append(this.phaseSecondsLeft / 60)
                                .append(" минут ")
                                .append((this.phaseSecondsLeft - this.phaseSecondsLeft / 60 * 60))
                                .append(" секунд");
                    }
                } else {
                    timeMessage.append(this.phaseSecondsLeft).append(" секунд");
                }

                this.arenaPlayers.forEach(player -> player.sendActionBar(timeMessage.toString()));
                if (printTime) {
                    this.broadcastMessage(timeMessage.toString());
                    this.broadcastSound(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2.0f);
                }
            }

            if (this.phaseSecondsLeft > 0) {
                Bukkit.getPluginManager().callEvent(new ArenaEverySecondTickEvent(this));
                if (this.state == GameState.GAME) this.onSecondTick();
                return;
            }

            switch (this.state) {
                case START:
                    this.startGame();
                    return;
                case GAME:
                    this.endGameByTimedOut();
                    if (this.state != GameState.END)
                        throw new IllegalStateException("Игра не была завершена при завершении времени арены");
                    return;
                case END:
                    this.restartGame();
                    return;
            }

        }, 20L, 20L);
    }

    public Plugin getPlugin() {
        return this.plugin;
    }

    public abstract void onSecondTick();

    public String getConnectDenyMessage() {
        if (this.state != GameState.START) {
            return ChatColor.RED + "Игра на арене уже началась";
        }
        if (this.arenaPlayers.size() >= this.maxStartPlayers) {
            return ChatColor.RED + "Арена заполнена";
        }
        return null;
    }

    public GameState getState() {
        return this.state;
    }

    public void broadcastSound(Sound sound, float pitch) {
        this.arenaPlayers.forEach(player -> player.playSound(player.getLocation(),
                sound, SoundCategory.MASTER,
                1.0f, pitch));
    }

    public void broadcastMessage(String message) {
        this.arenaPlayers.forEach(player -> player.sendMessage(message));
    }

    public void addPlayer(Player player) {
        String message = this.getConnectDenyMessage();
        if (message != null) {
            player.kickPlayer(message);
            return;
        }
        player.setFoodLevel(20);
        player.setHealth(20);
        player.setLevel(0);
        player.setGameMode(GameMode.ADVENTURE);
        player.setFireTicks(0);
        player.getInventory().clear();
        this.arenaPlayers.add(player);
        this.broadcastOnlineMessage(ChatColor.GREEN + "Игрок " + player.getName() + " присоединился к игре!");
        player.sendMessage(ChatColor.WHITE + "Для игры необходимо " + ChatColor.YELLOW + this.minStartPlayers + ChatColor.WHITE + " игроков");
        if (this.arenaPlayers.size() == this.minStartPlayers) {
            this.phaseSecondsLeft = this.minPlayersTimerSeconds + 1;
        } else if (this.arenaPlayers.size() == this.maxStartPlayers) {
            this.phaseSecondsLeft = Math.min(this.phaseSecondsLeft, this.maxPlayersTimerSeconds + 1);
        }
    }

    public void removePlayer(Player player) {
        if (!this.arenaPlayers.remove(player)) return;

        switch (this.state) {
            case START:
                this.broadcastOnlineMessage(ChatColor.YELLOW + "Игрок " + player.getName() + " покинул лобби ожидания!");
                player.kickPlayer(ChatColor.RED + "Вы покинули лобби!");
                if (this.arenaPlayers.size() == this.minStartPlayers - 1)
                    this.phaseSecondsLeft = -1;
                break;
            case GAME:
                Bukkit.broadcastMessage(ChatColor.YELLOW + "Игрок " + player.getName() + " отключился от игры!");
                player.kickPlayer(ChatColor.RED + "Вы покинули игру!");
                this.endGameByPlayerLeftIfRequired(player);
                if (this.arenaPlayers.size() < 1 && this.state == GameState.GAME)
                    throw new IllegalStateException("Игра не была завершена при выходе последнего игрока");
                break;
            case END:
                player.kickPlayer(ChatColor.RED + "Вы завершили игру!");
                break;
        }
    }

    private void broadcastOnlineMessage(String message) {
        ChatColor onlineColor;
        if (this.arenaPlayers.size() < this.minStartPlayers)
            onlineColor = ChatColor.RED;
        else if (this.arenaPlayers.size() < this.maxStartPlayers)
            onlineColor = ChatColor.YELLOW;
        else
            onlineColor = ChatColor.GREEN;
        this.broadcastMessage(message + " Онлайн: "
                + ChatColor.WHITE + "["
                + onlineColor + this.arenaPlayers.size()
                + ChatColor.WHITE + "/"
                + ChatColor.GREEN + this.maxStartPlayers
                + ChatColor.WHITE + "]");
    }

    public void startGame() {
        if (this.state == GameState.GAME)
            throw new IllegalArgumentException("Игра уже началась");
        if (this.state == GameState.END)
            throw new IllegalArgumentException("Невозможно начать игру до перезапуска арены");
        this.arenaPlayers.forEach(player -> player.setGameMode(GameMode.SURVIVAL));
        this.state = GameState.GAME;
        this.onGameStarted();
        this.phaseSecondsLeft = this.getGameDurationSeconds() + 1;
        this.broadcastSound(Sound.ENTITY_LIGHTNING_THUNDER, 1.0f);
        Bukkit.getPluginManager().callEvent(new ArenaStartEvent(this));
    }

    public void endGame() {
        if (this.state != GameState.GAME)
            throw new IllegalArgumentException("Игра не ведётся");
        Bukkit.getPluginManager().callEvent(new ArenaEndEvent(this));
        this.arenaPlayers.forEach(player -> player.setGameMode(GameMode.ADVENTURE));
        this.state = GameState.END;
        this.phaseSecondsLeft = this.endingDurationSeconds;
        this.onGameEnded();
        if (this.arenaPlayers.size() < 1) {
            this.restartGame();
        }
    }

    void restartGame() {
        new HashSet<>(this.arenaPlayers).forEach(player -> player.kickPlayer(ChatColor.RED + "Игра завершена!"));
        this.arenaPlayers.clear();
        this.state = GameState.START;
        this.phaseSecondsLeft = -1;
        this.prepareArena();
        this.plugin.getLogger().info("Арена подготовлена");
    }

    public int getMaxStartPlayers() {
        return maxStartPlayers;
    }

    protected abstract void prepareArena();

    protected abstract void onGameStarted();

    protected abstract void onGameEnded();

    protected abstract int getGameDurationSeconds();

    protected abstract void endGameByTimedOut();

    protected abstract void endGameByPlayerLeftIfRequired(Player player);

    public abstract void forceUnload();
}
