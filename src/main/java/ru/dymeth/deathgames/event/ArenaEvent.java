package ru.dymeth.deathgames.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import ru.dymeth.deathgames.arena.GameArena;

public abstract class ArenaEvent extends Event {
    private final GameArena arena;

    public ArenaEvent(GameArena arena) {
        this.arena = arena;
    }

    public <T extends GameArena> T getArena(Class<T> arenaClass) {
        return arenaClass.cast(this.arena);
    }

    @Override
    public abstract HandlerList getHandlers();
}
